package com.guillaumevdn.videomaker.util;

import java.util.Objects;

/**
 * @author GuillaumeVDN
 */
public class Pair<A, B> {

	// base
	private A a;
	private B b;

	public Pair() {
		this(null, null);
	}

	public Pair(A a, B b) {
		this.a = a;
		this.b = b;
	}

	// get
	public A getA() {
		return a;
	}

	public B getB() {
		return b;
	}

	// set
	public void setA(A a) {
		this.a = a;
	}

	public void setB(B b) {
		this.b = b;
	}

	// methods : object
	@Override
	public String toString() {
		return "(" + String.valueOf(a) + "," + String.valueOf(b) + ")";
	}

	@Override
	public int hashCode() {
		return Objects.hash(a, b);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		try {
			Pair<A, B> other = (Pair<A, B>) obj;
			return other != null && Objects.equals(a, other.a) && Objects.equals(b, other.b);
		} catch (ClassCastException ignored) {
			return false;
		}
	}

	// static
	public static <A, B> Pair<A, B> of(A a, B b) {
		return new Pair<>(a, b);
	}

	public static <A, B> Pair<A, B> empty() {
		return new Pair<>();
	}

}
