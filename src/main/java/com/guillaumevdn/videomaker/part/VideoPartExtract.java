package com.guillaumevdn.videomaker.part;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import com.drew.imaging.mp4.Mp4MetadataReader;
import com.drew.metadata.mp4.media.Mp4VideoDirectory;
import com.guillaumevdn.videomaker.util.Utils;
import com.guillaumevdn.videomaker.util.Utils.Text;

import net.bramp.ffmpeg.FFmpeg;

/**
 * @author GuillaumeVDN
 */
public class VideoPartExtract {

	// base
	private int id;
	private String name, episodeName;
	private String startTime, endTime;
	private int startMillis, endMillis;
	private String readableDurationTime;
	private String readableStartTime;
	private File episodeFile;
	private File extractFile;
	private File overlayFile;
	private File editFile;
	private File transcodeFile;

	public VideoPartExtract(int id, String name, String episodeName, File episodeFile, String startTime, String endTime, File extractFile, File overlayFile, File editFile, File transcodeFile) { // time format : hh:mm:ss.mmmm
		this.id = id;
		this.name = name;
		this.episodeName = episodeName;
		this.episodeFile = episodeFile;
		this.startTime = startTime;
		this.endTime = endTime;
		this.startMillis = Utils.decodeMillis(startTime);
		this.endMillis = Utils.decodeMillis(endTime);
		this.readableDurationTime = Utils.millisToReadableTimeString(endMillis - startMillis);
		this.readableStartTime = Utils.millisToReadableTimeString(startMillis);
		this.extractFile = extractFile;
		this.overlayFile = overlayFile;
		this.editFile = editFile;
		this.transcodeFile = transcodeFile;
	}

	// get
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEpisodeName() {
		return episodeName;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public int getStartMillis() {
		return startMillis;
	}

	public int getEndMillis() {
		return endMillis;
	}

	public String getReadableDurationTime() {
		return readableDurationTime;
	}

	public String getReadableStartTime() {
		return readableStartTime;
	}

	public File getEpisodeFile() {
		return episodeFile;
	}

	public File getExtractFile() {
		return extractFile;
	}

	public File getOverlayFile() {
		return overlayFile;
	}

	public File getEditFile() {
		return editFile;
	}

	public File getTranscodeFile() {
		return transcodeFile;
	}

	// methods
	public void extract(FFmpeg ffmpeg) throws Throwable {
		if (extractFile.exists()) throw new IllegalStateException("extract file already exists");
		Utils.runFFmpeg(ffmpeg,
				// time and input : don't change the order of those args ! otherwise some frames are missing or not in sync :think:
				"-ss", startTime,
				"-to", endTime,
				"-i", episodeFile.getAbsolutePath(),
				// codec
				"-c", "copy",
				"-avoid_negative_ts", "make_zero",
				// output
				extractFile.getAbsolutePath(),
				// misc
				"-v", "error"
				);
	}

	public void edit(FFmpeg ffmpeg, VideoPartFonts fonts720, VideoPartFonts fonts1080) throws Throwable {
		if (!extractFile.exists()) throw new IllegalStateException("extract file doesn't exist");
		if (overlayFile.exists()) throw new IllegalStateException("overlay file already exists");
		if (editFile.exists()) throw new IllegalStateException("edit file already exists");
		// read extract metadata
		Mp4VideoDirectory meta = Mp4MetadataReader.readMetadata(extractFile).getFirstDirectoryOfType(Mp4VideoDirectory.class);
		int width = meta.getInt(Mp4VideoDirectory.TAG_WIDTH);
		int height = meta.getInt(Mp4VideoDirectory.TAG_HEIGHT);
		// generate overlay
		VideoPartFonts fonts = width == 1280 ? fonts720 : fonts1080;
		BufferedImage textImage = Utils.generateTextImage(width, height,
				Text.of(name, fonts.getFontA(), fonts.getTextX(), fonts.getTextYA()),
				Text.of(episodeName + " (à " + readableStartTime + ")", fonts.getFontB(), fonts.getTextX(), fonts.getTextYB())
				//Text.of("à partir de " + Utils.millisToReadableTimeString(videoPart.getStartMillis()), fonts.getFontC(), fonts.getTextX(), fonts.getTextYC())
				);
		ImageIO.write(textImage, "png", overlayFile);
		// run ffmpeg
		Utils.runFFmpeg(ffmpeg,
				// time and input : don't change the order of those args ! otherwise some frames are missing or not in sync :think:
				"-i", extractFile.getAbsolutePath(),
				"-i", overlayFile.getAbsolutePath(),
				// filter
				"-filter_complex", "\"[0:v][1:v] overlay\"",
				// output
				editFile.getAbsolutePath(),
				// misc
				"-v", "error"
				);
	}

	public void transcode(FFmpeg ffmpeg) throws Throwable {
		if (!editFile.exists()) throw new IllegalStateException("edit file doesn't exist");
		if (transcodeFile.exists()) throw new IllegalStateException("transcode file already exists");
		Utils.runFFmpeg(ffmpeg,
				// input
				"-i", editFile.getAbsolutePath(),
				// codec
				"-c", "copy",
				"-bsf:v", "h264_mp4toannexb",
				"-f", "mpegts",
				// output
				transcodeFile.getAbsolutePath(),
				// misc
				"-v", "error"
				);
	}

	// cut parts : https://superuser.com/questions/377343/cut-part-from-video-file-from-start-position-to-end-position-with-ffmpeg
	// missing parts : https://superuser.com/questions/1167958/video-cut-with-missing-frames-in-ffmpeg?newreg=00ef08e282f94fb6b3afdc3dfe5b2b98

}
