package com.guillaumevdn.videomaker.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import net.bramp.ffmpeg.FFmpeg;

/**
 * @author GuillaumeVDN
 */
public final class Utils {

	public static int decodeMillis(String time) {
		int millis = 0;
		String[] split = time.trim().split(":");
		if (split.length == 3) {
			millis += Integer.parseInt(split[0]) * 60 * 60 * 1000;
			millis += Integer.parseInt(split[1]) * 60 * 1000;
			millis += decodeSecondsMillis(split[2]);
		} else if (split.length == 2) {
			millis += Integer.parseInt(split[0]) * 60L * 1000;
			millis += decodeSecondsMillis(split[1]);
		} else {
			throw new IllegalArgumentException("invalid time string '" + time + "'");
		}
		return millis;
	}

	public static int decodeSecondsMillis(String time) {
		int millis = 0;
		String[] split = time.split("\\.");
		if (split.length == 1) {
			millis += Integer.parseInt(split[0]) * 1000;
		} else if (split.length == 2) {
			millis += Integer.parseInt(split[0]) * 1000;
			String more = split[1];
			while (more.length() < 4) more += "0";
			millis += Integer.parseInt(more);
		} else {
			throw new IllegalArgumentException("invalid time string '" + time + "'");
		}
		return millis;
	}

	public static String millisToTimeString(int millis) {
		String string = formatNumber(millis % 1000, 4);
		int seconds = (int) Math.floor((double) millis / 1000);
		if (seconds < 60) {
			string = formatNumber(seconds, 2) + "." + string;
		} else {
			string = formatNumber(seconds % 60, 2) + "." + string;
			int minutes = (int) Math.floor((double) seconds / 60);
			if (minutes < 60) {
				string = formatNumber(minutes, 2) + ":" + string;
			} else {
				string = formatNumber(minutes % 60, 2) + ":" + string;
				int hours = (int) Math.floor((double) minutes / 60);
				if (hours > 0) {
					string = formatNumber(hours, 2) + ":" + string;
				}
			}
		}
		return string;
	}

	public static String millisToReadableTimeString(int millis) {
		String string;
		int seconds = (int) Math.floor((double) millis / 1000);
		if (seconds < 60) {
			string = formatNumber(seconds, 2) + "s";
		} else {
			string = formatNumber(seconds % 60, 2) + "s";
			int minutes = (int) Math.floor((double) seconds / 60);
			if (minutes < 60) {
				string = formatNumber(minutes, 2) + "m" + string;
			} else {
				string = formatNumber(minutes % 60, 2) + "m" + string;
				int hours = (int) Math.floor((double) minutes / 60);
				if (hours > 0) {
					string = formatNumber(hours, 2) + "h" + string;
				}
			}
		}
		return string.startsWith("0") ? string.substring(1) : string;
	}

	public static String millisToSecondsString(int millis) {
		return ((int) Math.floor((double) millis / 1000)) + "." + formatNumber(millis % 1000, 4);
	}

	public static String formatNumber(int number, int length) {
		return formatNumber(String.valueOf(number), length);
	}

	private static String formatNumber(String number, int length) {
		while (number.length() < length) {
			number = "0" + number;
		}
		return number;
	}

	public static String keepAlphanumeric(String string) {
		StringBuilder builder = new StringBuilder(string.length());
		for (char ch : string.toCharArray()) {
			ch = Character.toLowerCase(ch);
			if ((ch >= 'a' || ch <= 'z') && (ch >= '0' || ch <= '9')) {
				builder.append(ch);
			}
		}
		return builder.toString();
	}

	public static void deleteFile(File file) {
		if (file.exists()) {
			if (file.isDirectory()) {
				for (File sub : file.listFiles()) {
					deleteFile(sub);
				}
			}
			file.delete();
		}
	}

	public static void runFFmpeg(FFmpeg ffmpeg, String... args) throws IOException {
		ffmpeg.run(Arrays.asList(args));
	}

	// skyrim font : Futura Condensed Regular
	public static BufferedImage generateTextImage(int width, int height, Text... texts) throws Throwable {
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = bufferedImage.createGraphics();
		graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		graphics.setColor(Color.WHITE);
		for (Text text : texts) {
			graphics.setFont(text.font);
			graphics.drawString(text.text, text.x, text.y);
		}
		return bufferedImage;
	}

	public static final class Text {
		private String text;
		private Font font;
		private int x, y;
		private Text() {}
		public static Text of(String text, Font font, int x, int y) {
			Text txt = new Text();
			txt.text = text;
			txt.font = font;
			txt.x = x;
			txt.y = y;
			return txt;
		}
	}

}
